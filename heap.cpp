#include <iostream>
#include<vector>
#include <omp.h>


using namespace std;


void set(int* array, int len, int index)
{
    int left = 2*index + 1;
    int right = 2*index + 2;
    int max = index;

    if(left<len && array[left] > array[max])
        max = left;

    if(right<len && array[right] > array[max])
        max = right;

    if(max != index)
    {
        swap(array[max], array[index]);
        set(array, len, max);
    }

}

void heap_sort(int* array, int size)
{
#pragma omp parallel
    for(int i=size/2 - 1; i >= 0; i--)
    {
#pragma omp single
        set(array, size, i);
    }
#pragma omp parallel
    for(int i = size - 1; i >= 1; i--)
    {
#pragma omp single
        swap(array[0], array[i]);
#pragma omp single
        set(array, i, 0);
    }
}


int main()
{
    int size, threads;
    int array[1000] ;

    cout<<"Enter array length:";
    cin>>size;
    cout<<"Enter number of threads:";
    cin>>threads;
    cout << "Create array" << endl;
    for (int i = 0; i < size; ++i)
    {
        array[i] = rand() % 100;
        cout << array[i] << "  ";
    }
    cout << endl;

#pragma omp paralle for num_threads(threads)

    double start = omp_get_wtime();

    heap_sort(array, size);

    double end = omp_get_wtime();

    cout << "Sorted array:" << endl;
    for (int i = 0; i < size; ++i)
        cout << array[i] << "  ";
    cout << endl;

    cout<<"Time_start: "<<start;
    cout<<" Time_ends: "<<end<<endl;

    return 0;

}