#include <stdlib.h>
#include <omp.h>
#include <iostream>

using namespace std;

unsigned int compare_time = 0;
unsigned int LeoNum[] = {
        1, 1, 3, 5, 9, 15, 25, 41, 67, 109, 177, 287, 465, 753, 1219, 1973,
        3193, 5167, 8361, 13529, 21891, 35421, 57313, 92735, 150049, 242785,
        392835, 635621, 1028457, 1664079, 2692537, 4356617, 7049155, 11405773,
        18454929, 29860703, 48315633, 78176337, 126491971, 204668309, 331160281,
        535828591, 866988873, 1402817465, 2269806339u, 3672623805u,
};


int compare(int& t1, int& t2)
{
    compare_time += 1;
    return t1 < t2;
}


void sorts(int* array, int low, int id, int* top)
{
    int el_1, el_2;

    while (id > 0)
    {
        int last = low - LeoNum[top[id]];

        if (compare(array[low], array[last]))
        {
            if (top[id] > 1)
            {
                el_1 = last - 1 - LeoNum[top[id] - 2];
                el_2 = low - 1;

                if (compare(array[last], array[el_1]))
                    break;

                if (compare(array[last], array[el_2]))
                    break;
            }
            swap(array[low], array[last]);
            low = last;
            id -= 1;
        }
        else
            break;
    }

    int layer = top[id];

    while (layer > 1)
    {
        int max = low;
        el_1 = low - 1 - LeoNum[layer - 2];
        el_2 = low - 1;

        if (compare(array[max], array[el_1]))
            max = el_1;

        if (compare(array[max], array[el_2]))
            max = el_2;

        if (max == el_1)
        {
            swap(array[low], array[el_1]);
            low = el_1;
            layer -= 1;
        }
        else if (max == el_2)
        {
            swap(array[low], array[el_2]);
            low = el_2;
            layer -= 2;
        }
        else break;
    }

}

void smooth_sort(int* array, int size)
{
    int top[64] = {1};
    int layer = 0;

    for (int i = 1; i < size; i++)
    {
#pragma omp parallel sections
        {
#pragma omp section

            if (layer > 0 && top[layer - 1] - top[layer] == 1)
            {
                layer -= 1;
                top[layer] += 1;
            }
            else if (top[layer] != 1)
            {
                layer += 1;
                top[layer] = 1;
            }
            else
            {
                layer += 1;
                top[layer] = 0;
            }

#pragma omp section
            sorts(array, i, layer, top);
        }
    }

    for (int i = size - 2; i > 0; i--)
    {
        if (top[layer] <= 1)
            layer -= 1;
        else
        {
            top[layer] -= 1;
            top[layer + 1] = top[layer] - 1;
            layer += 1;

#pragma omp parallel sections
            {
#pragma omp section
                sorts(array, i - LeoNum[top[layer]], layer - 1, top);
#pragma omp section
                sorts(array, i, layer, top);
            }
        }
    }
}


int main()
{
    int size, threads;
    int array[1000] ;

    cout<<"Enter array length:";
    cin>>size;
    cout<<"Enter number of threads:";
    cin>>threads;
    cout << "Create array" << endl;

    for (int i = 0; i < size; ++i)
    {
        array[i] = rand() % 100;
        cout << array[i] << " ";
    }

    cout << endl;
    double start = omp_get_wtime();
#pragma omp paralle for num_threads(threads)

    smooth_sort(array, size);

    double end = omp_get_wtime();
    for (int i = 0; i<size; i++)
        cout << array[i] << " ";

    cout << endl;
    cout<<"Time_start: "<<start;
    cout<<" Time_ends: "<<end<<endl;

    return 0;
}
